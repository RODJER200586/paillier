from django.core.management.base import BaseCommand, no_translations
from purchases.models import Purchase
from random import randrange, choice
from string import ascii_lowercase
from django.conf import settings
from phe import EncryptedNumber, PaillierPublicKey, PaillierPrivateKey


class Command(BaseCommand):

    @no_translations
    def handle(self, *args, **options):
        public_key = PaillierPublicKey(
            n=settings.PUBLIC_KEY_N,
        )
        private_key = PaillierPrivateKey(
            public_key=public_key,
            p=settings.PRIVATE_KEY_P,
            q=settings.PRIVATE_KEY_Q,

        )

        a = public_key.encrypt(100).ciphertext()
        b = public_key.encrypt(90).ciphertext()
        ea = EncryptedNumber(public_key, a)
        eb = EncryptedNumber(public_key, b)

        es = ea + eb
        s = es.ciphertext()
        esn = EncryptedNumber(public_key, s)

        print(private_key.decrypt(esn))
