from django.db import models


class Purchase(models.Model):
    name = models.CharField(max_length=1024, null=False, blank=False)
    price = models.BigIntegerField(null=False, blank=False)
    encoded_price = models.CharField(max_length=2048, null=False, blank=False)
    created_at = models.DateTimeField(auto_now_add=True)
