from django.core.management.base import BaseCommand, no_translations
from purchases.models import Purchase
from random import randrange, choice
from string import ascii_lowercase
from django.conf import settings
from phe import PaillierPublicKey


class Command(BaseCommand):
    help = 'Generation of purchases'

    @no_translations
    def handle(self, *args, **options):
        public_key = PaillierPublicKey(
            n=settings.PUBLIC_KEY_N,
        )
        for _ in range(50):
            name = ''.join(choice(ascii_lowercase) for i in range(30))
            price = randrange(1000)
            encoded_price = public_key.encrypt(price).ciphertext()
            Purchase.objects.create(
                name=name,
                price=price,
                encoded_price=str(encoded_price),
            )
