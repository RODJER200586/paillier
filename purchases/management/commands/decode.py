from django.core.management.base import BaseCommand, no_translations
from purchases.models import Purchase
from random import randrange, choice
from string import ascii_lowercase
from django.conf import settings
from phe import EncryptedNumber, PaillierPublicKey, PaillierPrivateKey


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('--value', nargs='?', type=str)

    @no_translations
    def handle(self, *args, **options):
        value = options['value']
        public_key = PaillierPublicKey(
            n=settings.PUBLIC_KEY_N,
        )
        private_key = PaillierPrivateKey(
            public_key=public_key,
            p=settings.PRIVATE_KEY_P,
            q=settings.PRIVATE_KEY_Q,

        )
        print(private_key.decrypt(EncryptedNumber(public_key, int(value))))
